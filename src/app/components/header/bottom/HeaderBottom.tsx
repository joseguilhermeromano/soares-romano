import React from "react";

const HeaderBottom: React.FC = () => {
  return (
    <div className="header-bottom">
        <div className="container">
            <div className="row d-flex align-items-center">
                <div className="col-xl-2 col-lg-2 col-md-4">
                    <div className="logo">
                        <a href="/">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKMAAAAkCAYAAADl2YrgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDYuMC1jMDA2IDc5LjE2NDc1MywgMjAyMS8wMi8xNS0xMTo1MjoxMyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDphYjRlMTlhNC00OGU0LWM4NDMtYjM5NC05MmExM2Q4YjIyM2EiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RjREM0Q1RTMzNEIyMTFFRDkxNDVBNkIzNjQxQTAxMzEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RjREM0Q1RTIzNEIyMTFFRDkxNDVBNkIzNjQxQTAxMzEiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTkyZDU3M2ItZGY2ZS0xZjRjLThkY2MtMDM0Y2QwMDBlMmY2IiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6YjgxMjUyOGQtNDY0Mi0xOTQyLTljN2EtYWExYWE0MmFjY2ZmIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+HTFS4AAABcZJREFUeNrsXD9vGzcU59lyXSQtqqFDPPXSImt96dbJ571AlKwZIn8CWUtXS1O7yfkEUoaMrW3kA+i8tFMBZa3RWlniAO0gI6hR17ZUPuldwxJHHsnT6XgKH0BIInnHe+RP77/k/fLio2NCyF3arogGjceErK8TsrGxQp4/v5q2O/Q99MGYJq3RdvrDT5dbskme5xFHxdOjrz+o0pc6bQ+w69mPP//TU7xmk7ZXtO3Ta0bsnApt92jbsIDHijtmK4EX0peANgDTJ/g+5KYdKwCxj9fG1KD99ykghywALizh+0JhY+JN0aEhy7AjberP4R4dDogEzxH6H7JgtEX3eYpMhZr3bdPWcpgqlGoq/StunxwtgJS02fsARqeisxHYdR60DHt5KOgflNlpaDLfsgDVdhKBl/aQbmDksJSN6B6ygPENb9MWqOp2acHIbgx1ZmRTew6Idp0bPa9t8KARlHA2T2n/YZklI0uBZOzIQcA6QEYIwqW0GavuiJeLnDftyBpyWQ9NwmxCiGbCJiOhwWl6CR4ibwsZrFGPnQV6r1bC2luMmTLEdQ+zBvcx2xJyfME9jyUecSb+0L4fOjDqH1SDiAO4JB6jc2FzmyqgZLJKPoKgxnmtLToHPu/hmMg86dB5kO9tGvC2i7yJvGUAT9dw39L4ixDwDoyKGwohpF2NS2CzD+h1bVaycfdsIcDS1m4hUFRs5F2QnnTNHQ0p309xBk33TIm//9mMUGGTpd3czG60ukpvtpKtWQrErgCI8G3u0bZPxMHgPXp9LeMj7Gk6a3WVNVHankqAOECpFZEFJQ4qUPJlQnEJ2Ye3Zp//uiDk8vLd2BKp5jrXnRRQb9K5BwIV3pmTvRWD45y2zxKei6WGbE2UiAcCkMM6O1ywO96Lfq5ghHpEE7q+JuQ2BeLH92bXn/w5npY6QN/N9dJo6FAQUkqSFE0BGH2wm/jDRWANENyybNIQwRFx4HiK4EgCVIjqeiSRtkkSEeZvJ10H66ckGoghf+/ACEWx86Dvv10n7xH5PCDBI0THxReAesAfLgMs2Vo7SdkkzGr0JLZsQBKCzKieRdc0JQDWIg3+soNxtTJTzSd/jKdAbHy3Tt7+Op72VZbHLRpJpJWo3xdI0zzoSBeMqMITeU2r1s7dZrxjqKbB4ZjaiJPZ5/OTMTl7PZ72mTojn35pHRjhcJ5wKm0/h2Ld0FT6SKROVRZ+EqjVRZo783NgYsDdRgfmb3RgsoDRNkKVdZ8J0mYOaBfskAVEHEt8WfTzVbJ4vrLQzjJRkvrCgw3Rsw0Y1Wi7rSvz1osFI3GkK1niLEwZCzUCA/vYgdEyEIJE6Zrado4cGHXANmFUsseN1RCIVYlEidDmekLMq6EdGN0WSEEKkvBAMAwedZu1J+n8rRKD0XdgtANwIupKjP3teQWIF0hDydhm0Q/nimsFRj2qZ5G02CkhENPAGDowFk8NXc8zIc+cJ81tLUzRib5EATpqi6aBAyP5r6jUn9O9qnnYXTlIYFnQviPhL5cYKstfmcH4QDI2UgBPiyhUkgiu7SbYnf0MYNw0fA4TgDyTjNXwC5q0Tj+D9Fbiz6ML/UZfP9fW75ibfnM2Jo8fr03b2Vmm3PTvX33z9gvpw3pevDEQQpFVXrclY5AxEQat49AOpgC7KYAfIADTQAjqMf6nrh6b28bD76jwwv0exkdPP1BYM+KqaPopNiLwdcTsVz2FPygwPs/KH3jTE0sk3URBEqRtYkx7hs8w4tRZh4jji1UNoz9k5kaxI8HGNxV5aWnsQ8jNiZj38M9fpxLeAqKX2tzl1jHiD2TYLUvAaMNzDDhbRufHTRFKCOsJedsmeinAHpnjLwSTCCTjCW1Qm31V4P6s4TfVtkPrYYmWTELCgcJfdbSS7C2LAQnFuXeRt3pKOGj6S0e0s2t5PZM3mUzKsn+F/Y0y2mdwCFsMKMEeK3VJWQp/oCWOF8nfvwIMAJraLz2EE+ejAAAAAElFTkSuQmCC" alt="Logo" />
                        </a>
                    </div>
                </div>
                <div className="col-xl-10 col-lg-10 col-md-8">
                    <div className="main-menu text-sm-end">
                        <nav>
                            <ul>
                                <li><a href="/themeforest/react/landi">Home</a>
                                </li>
                                <li>
                                    <a href="/themeforest/react/landi/about">About</a>
                                </li>
                                <li>
                                    <a href="/themeforest/react/landi">Services</a>
                                </li>
                                <li>
                                    <a href="/themeforest/react/landi">Page</a>
                                </li>
                                <li><a href="/themeforest/react/landi">Blog</a>
                                </li>
                                <li>
                                    <a href="/themeforest/react/landi/contact">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
};

export default HeaderBottom;

import React from "react";
import FacebookIcon from '@mui/icons-material/Facebook';
import AccessTimeFilledIcon from '@mui/icons-material/AccessTimeFilled';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import InstagramIcon from '@mui/icons-material/Instagram';
import EmailIcon from '@mui/icons-material/Email';
import Tooltip from '@mui/material/Tooltip';

const HeaderTop: React.FC = () => {
  return (
    <div className="header-top pt-15 pb-15 theme-bg-1">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <div className="header-info text-sm-start text-xs-center">
                  <AccessTimeFilledIcon  style={{color: "white"}} strokeWidth="0"/> <strong style={{color: "white"}}>Mon-Fri 09:00 - 18:00</strong>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <div className="header-social text-sm-end text-xs-center">
              <Tooltip title="+1 (732) 337-1831" arrow>
                <a href="https://wa.me/17323371831">
                  <WhatsAppIcon  fill="currentColor"  stroke="currentColor" strokeWidth="0"/>
                </a>
                </Tooltip>
              <Tooltip title="/familiagordura" arrow>
                <a href="https://www.facebook.com/familiagordura"><FacebookIcon  fill="currentColor"  stroke="currentColor" strokeWidth="0"/></a>
              </Tooltip>
              <a href="https://instagram.com"><InstagramIcon  fill="currentColor"  stroke="currentColor" strokeWidth="0"/></a>
              <Tooltip title="familiagordura1379@gmail.com" arrow>
                <a href="mailto:familiagordura1379@gmail.com"><EmailIcon  fill="currentColor"  stroke="currentColor" strokeWidth="0"/></a>
              </Tooltip>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderTop;

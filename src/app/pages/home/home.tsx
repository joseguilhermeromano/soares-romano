import React, { useEffect } from 'react';
import HeaderTop from '../../components/header/top/HeaderTop';
import HeaderBottom from '../../components/header/bottom/HeaderBottom';

function Home() {
    
    useEffect(() => {
        document.title = 'Home - Soares & Romano';
    }, []);

  return (
    <>
        <HeaderTop />
        <HeaderBottom />
    </>
  );
}

export default Home;
